const lexi = require('lexicographic-integer')

function pack (intNr) {
  if (!Number.isInteger(intNr)) {
    throw new NotAnIntegerError(`'${intNr}' is not a valid integer`)
  }
  return lexi.pack(intNr, 'hex')
}

function unpack (hexTxtNr) {
  const res = lexi.unpack(hexTxtNr, 'hex')
  if (res === undefined) {
    throw new NotUnpackableError(`'${hexTxtNr}' is not unpackable to an integer`)
  }
  return res
}

function * generator (startI = 0) {
  let i = startI
  while (true) {
    yield pack(i++)
  }
}

function getNext (hexTxtNr) {
  return pack(unpack(hexTxtNr) + 1)
}

class NotAnIntegerError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

class NotUnpackableError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
  }
}

module.exports = {
  pack,
  unpack,
  generator,
  getNext
}
