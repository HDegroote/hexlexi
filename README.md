# Hexlexi

Hexadecimal lexicographic integers: positive integers represented as hexadecimal strings, lexicographically ordered low-to-high.

Builds on https://www.npmjs.com/package/lexicographic-integer

## Install
`npm add hexlexi`

## Usage

```
const hexlexi = require('hexlexi')

hexlexi.pack(10) // '0a'

hexlexi.unpack('0a') // 10

const generator = hexlexi.generator()
for (let i = 0; i < 4; i++) {
  console.log(generator.next().value)
} // '00', '01', '02', '03'

hexlexi.getNext('00') // '01'
```

## API
`hexlexi.pack(intNr)`

Get the lexicographical representation of the given integer.

`hexlexi.unpack(hexTxtNr)`

Get the integer representation of the given hexadecimal string.

`hexlexi.generator(startI=0)`

Get a generator of hexadecimal strings incrementing one-by-one, optionally choosing with which number to start (0 default).

`hexlexi.getNext(hexTxtNr)`

Get the lexicographical representation of the number just after the given hexadecimal string
